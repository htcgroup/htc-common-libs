/*
Copyright 2022 Hieu Tran

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
        You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

plugins {
    id 'java-library'
    id 'signing'
    id 'maven-publish'
    id 'io.github.gradle-nexus.publish-plugin' version '1.1.0'
}

group 'com.gitlab.htcgroup'
version '0.0.1-SNAPSHOT'

java {
    withJavadocJar()
    withSourcesJar()
}

publishing {
    publications {
        mavenJava(MavenPublication) {
            from(components.java)

            pom {
                name = 'htc-common-libs'
                description = 'HTC Common Libraries'
                url = 'https://gitlab.com/htcgroup/htc-common-libs'
                licenses {
                    license {
                        name = 'The Apache License, Version 2.0'
                        url = 'http://www.apache.org/licenses/LICENSE-2.0.txt'
                    }
                }
                developers {
                    developer {
                        id = 'tranminhhieu303200'
                        name = 'Hieu Tran'
                    }
                }
                scm {
                    url = 'https://gitlab.com/htcgroup/htc-common-libs'
                    connection = 'scm:git://github.com/htcgroup/htc-common-libs.git'
                    developerConnection = 'scm:git://github.com/htcgroup/htc-common-libs.git'
                }
            }
        }
    }
}

nexusPublishing {
    repositories {
        sonatype {
            nexusUrl.set(uri('https://s01.oss.sonatype.org/service/local/'))
            snapshotRepositoryUrl.set(uri('https://s01.oss.sonatype.org/content/repositories/snapshots/'))
        }
    }
}

signing {
    sign publishing.publications.mavenJava
}

ext.genOutputDir = file("$buildDir/generated-resources")

task generateVersionTxt() {
    ext.outputFile = file("$genOutputDir/version.txt")
    outputs.file(outputFile)
    doLast {
        outputFile.text = """
            GroupId: ${project.group}
            Name: ${project.name}
            Version: $version
            Build-time: ${java.time.LocalDateTime.now()}
        """
    }
}

sourceSets.main.output.dir genOutputDir, builtBy: generateVersionTxt

repositories {
    mavenCentral()
    mavenLocal()
}

dependencies {
    testImplementation 'org.junit.jupiter:junit-jupiter-api:5.8.1'
    testRuntimeOnly 'org.junit.jupiter:junit-jupiter-engine:5.8.1'
}

test {
    useJUnitPlatform()
    testLogging {
        events 'passed', 'skipped', 'failed'
    }
}
