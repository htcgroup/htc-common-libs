package com.gitlab.htcgroup.common.snowflake;

import org.junit.jupiter.api.Test;

class SnowflakeTest {
    
    @Test
    public void testGenerateIdSnowflake() {
        Snowflake snowflake = new Snowflake(1, 1665853200000L);
        long id = snowflake.nextId();
        System.out.println(id);
    }
}
